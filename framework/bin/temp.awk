BEGIN {
  FS="-"
}
{
  level=(NF-1)/2
  path[$level]=$NF
  del="/"
  temp=""
  for (i in path) {
    #print level " --> " path[$level] " << " $0
    #print $0
    # Root Level or Heading 1
    if (level == 1) {
      basePath=path[$level]
      fullpath=basePath
      pastPath=basePath
    } else if ( level == pastLevel) {
      fullpath=basePath del path[$level]
    } else if (level > pastLevel) {
      basePath=pastPath
      fullpath=basePath del path[$level]
    } else if ( level < pastLevel) {
      # Problematic
      split(fullpath,comp,"/")
      temp=""
      for (i=1; i<=level-1;i++) {
        if (i == 1) { temp=comp[i] }
        else { temp=temp del comp[i] }
      }
      basePath=temp
      fullpath=basePath del path[$level]
    } else {
      print "ERROR: No Logic for this case : " $0
    }
    print fullpath
    pastLevel=level
    pastPath=fullpath
  }
}
